class Bombe extends React.Component{
  constructor(props)
  {
      super(props)
      this.state={minutes:3,secondes:59,explosion:false,vitesseTimer:1000,filAqua:true}
  }
  static getDerivedStateFromProps(props, state) {
      if(state.minutes===0&&state.secondes===0)
      {
          if(state.idTimer)
          {
              clearInterval(state.idTimer);
          }
          return {explosion:true,idTimer:null}
      }
      else if(state.secondes<=0)
      {
          return{minutes:state.minutes-1,secondes:59-state.secondes}
      }
      else if(state.secondes>=60)
      {
          console.log(state.secondes)
          return{minutes:state.minutes+1,secondes:(0+(state.secondes-60))}
      }
      return{state}
    }
  render()
  {
    return (  <React.Fragment>
           
          <div className="row d-flex justify-content-center">
          
          <br/>
              <div className="col-md-6 col-lg-4">
              <div className="row justify-content-center"> 
                  <h2>{this.state.explosion===false ? "Tic tac ..." : "BOOM"}</h2>
              </div>
              <div className="row justify-content-center"> 
                  {this.state.explosion===false ? "" : <img className="img-fluid col-md-6" src="./img/explosion.gif" />}
              </div>
              <div className="row justify-content-center"> 
              <div className="col-lg-3 col-md-4">
          <input disabled type="text"  className="form-control bg-dark text-danger" value={+this.state.minutes+":"+this.state.secondes}/>
          </div>
          </div>
          <div className="row d-flex flex-column">
          <br/>
          <button className="btn btn-success m-2"  onClick={this.couperFilVert}>Couper le fil vert (Stop)</button>
          <br/>
          <button className="btn btn-danger m-2"  onClick={this.couperFilRouge}>Couper le fil rouge (Explosion)</button>
          <br/>
          <button className="btn btn-primary m-2" value={15} onClick={this.couperFilBleu}>Couper le fil bleu (Aléatoire)</button>
          <br/>
          <button className="btn btn-warning m-2" onClick={this.couperFilJaune}>Couper le fil jaune (X2)</button>
          <br/>
          <button className="btn btn-info m-2" onClick={this.couperFilAqua}>Couper le fil Aqua (Frozen)</button>
          <br/>
          </div>
          </div>
          </div>
      </React.Fragment>)
  }
  componentDidMount() {
      let idTimer=setInterval(() => {
          let lesSecondes=this.state.secondes;
            this.setState({secondes:lesSecondes-1})
        
        
      }, this.state.vitesseTimer)
      this.setState({idTimer:idTimer});
    }

    shouldComponentUpdate(){
        return this.state.filAqua
    }

    couperFilRouge=(e)=>
    {
        this.setState({minutes:0,secondes:0})
    }
    couperFilVert=(e)=>
    {
        if(!!this.state.idTimer)
        {
            clearInterval(this.state.idTimer)
        }
    }
 couperFilBleu=(e)=>
 {
     let temps= parseInt(e.target.value)
     let lesSecondes=this.state.secondes;
     let plusOuMoins=Math.floor(Math.random()*2)
      if(plusOuMoins===0)
      {
          this.setState({secondes:lesSecondes-temps})
      }
      else{
          this.setState({secondes:lesSecondes+temps})
      }
      console.log(this.state.secondes)
 }
 couperFilJaune=(e)=>
 {
  if(!!this.state.idTimer)
  {
      clearInterval(this.state.idTimer)
  }
  let vitesse= this.state.vitesseTimer>1? this.state.vitesseTimer/2 : 1;
  console.log(vitesse)
  let newTimer=setInterval(()=>{
      let lesSecondes=this.state.secondes;
      this.setState({secondes:lesSecondes-1})
  },vitesse)
  this.setState({idTimer:newTimer,vitesseTimer:vitesse});
 }
couperFilAqua=(e)=>
{
    this.setState({filAqua:!this.state.filAqua});
}

}


ReactDOM.render(<Bombe />,document.getElementById('root'))

